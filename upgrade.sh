#!/bin/bash

PARENT_DIR="$(dirname "$(pwd -P)")";

if [ $1 == "bump" ]
  then
    echo "pushing new version to git"
    git add . && yarn version --patch  && git push
    echo "waiting for package to be published"
    sleep 45s
fi
echo $PARENT_DIR
#iterate over all directories in parent directory
for dir in "$PARENT_DIR"/*; do
  echo $dir
  if [ -d "$dir" ]; then
    echo "upgrading $dir"
    cd "$dir"
    yarn upgrade @cslaf/sd-types &
  fi
done
wait # wait for all background jobs to finish
echo "done"
