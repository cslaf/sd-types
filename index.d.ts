/**
 * Stringified UUIDv4.
 * See [RFC 4112](https://tools.ietf.org/html/rfc4122)
 * @pattern [0-9A-Fa-f]{8}-[0-9A-Fa-f]{4}-4[0-9A-Fa-f]{3}-[89ABab][0-9A-Fa-f]{3}-[0-9A-Fa-f]{12}
 * @format uuid
 */
type UUID = string;

interface IUserRating {
  rateable_id: UUID;
  rating: number;
  notes?: string;
  public?: boolean;
  rateable_type: string;
}

interface IRateable {
  rating_count?: number;
  rating_average?: number;
  my_rating?: IUserRating;
}

/**
 * All the array properties are min,max except prompt and tags
 *
 * @interface ImageFilterRequest
 *
 */
interface ImageFilterRequest {
  height?: string[];
  width?: string[];
  prompt?: string[];
  info?: string;
  bucket?: string;
  steps?: number[];
  sampler?: string;
  model_hash?: string;
  model_name?: string;
  batch_size?: number[];
  batch_pos?: number;
  denoising_strength?: number[];
  rating_average?: number[];
  start_date?: Date;
  end_date?: Date;
  tags?: string[];
  unsafe?: boolean;
  favorites?: boolean;
  page?: number;
  page_size?: number;
  last_created_at?: Date;
  batch_id?: UUID[];
  order_by?: string[];
  seed?: string[];
  is_public?: boolean;
  sd_model_checkpoint?: string;
  random_seed?: number;
}

// Filter request for the image/best endpoint
interface BestFilterRequest extends ImageFilterRequest {
  last_rating_average?: number;
}

interface CheckpointWithMetadata {
  model_name: string;
  display_name: string;
  title?: string;
  hash?: string;
  type?: string;
  machine_id?: string;
  filename?: string;
  trained_words?: string[];
  download_url?: string;
  vae_download_url?: string;
  id?: UUID;
  model?: CivitaiModelVersion;
}

type WorkerStatus = "idle" | "busy" | "error" | "offline" | "init" | "pending";

/**
 * @interface BaseRequest
 * Shared properties between all requests
 * @property {UUID} user_id
 * @property {UUID} batch_id
 * @property {string} type - The type of request
 * @property {string} [sd_model_checkpoint] - The model checkpoint to use
 * @property {string} [info] - Iformation after the generation has finished
 * @property {boolean} [hide] - Set image to be private
 * @property {string} [model_hash] - The model hash
 */
interface BaseRequest extends IRateable {
  user_id: UUID;
  batch_id: UUID;
  type: "txt2img" | "img2img" | "upscale";
  sd_model_checkpoint?: any;
  info?: string;
  hide?: boolean;
  model_hash?: string;
  groups?: Group[];
  image_groups?: ImageGroup[];
  extra_generation_params?: any;

  alwayson_scripts?: AlwaysonScripts;
  references?: (ImageReference | RawImage)[];
}

interface UpscaleRequest extends BaseRequest {
  resize_mode?: number;
  show_extras_results?: boolean;
  gfpgan_visibility?: number;
  codeformer_visibility?: number;
  codeformer_weight?: number;
  upscaling_resize?: number;
  upscaling_resize_w?: number;
  upscaling_resize_h?: number;
  upscaling_crop?: boolean;
  upscaler_1?: string;
  upscaler_2?: string;
  extras_upscaler_2_visibility?: number;
  upscale_first?: boolean;
  image?: string;
}

interface OverrideSettings {
  [key: string]: any;
  clip_skip?: number;
}

interface Any2imgRequest extends BaseRequest {
  prompt: string;
  negative_prompt?: string;
  sampler_index?: string;
  width: number;
  height: number;
  denoising_strength?: number;
  batch_size?: number;
  n_iter?: number;

  styles?: string[];

  steps: number;
  cfg_scale: number;

  restore_faces?: boolean;
  tiling?: boolean;
  override_settings?: any;

  seed?: string;
  subseed?: string;

  seed_resize_from_h?: number;
  seed_resize_from_w?: number;

  s_churn?: number;
  s_tmax?: number;
  s_tmin?: number;
  s_noise?: number;

  hr_scale?: number;
  hr_sampler_name?: string;
  hr_upscaler?: string;
  hr_second_pass_steps?: number;
  hr_resize_x?: 0;
  hr_resize_y?: 0;
  hr_prompt?: string;
  hr_negative_prompt?: string;
}

interface Txt2imgRequest extends Any2imgRequest {
  enable_hr?: boolean;
  firstphase_width?: number;
  firstphase_height?: number;
}

interface Img2imgRequest extends Any2imgRequest {
  init_images?: (ImageReference | RawImage | string)[];
  resize_mode?: number;
  mask?: string;
  denoising_strength?: number;
  mask_blur?: number;
  inpainting_fill?: number;
  inpaint_full_res?: boolean;
  inpaint_full_res_padding?: number;
  inpainting_mask_invert?: number;
  include_init_images?: boolean;
}

interface RawImage {
  height: number;
  width: number;
  name: string;
  data: string;
  relationship_type?:
    | "img2img"
    | "upscale"
    | "controlnet_preprocess"
    | "controlnet_postprocess";
}

interface ImageUpload {
  id?: UUID;
  user_id?: UUID;
  user?: User;
  filepath: string;
  name: string;
  height: number;
  width: number;
  relationship_type?:
    | "img2img"
    | "upscale"
    | "controlnet_preprocess"
    | "controlnet_postprocess";
}

interface ImageReference {
  image_id: UUID;
  image?: Image;
  reference_id: UUID;
  reference_type: "upload" | "image";
  relationship_type:
    | "img2img"
    | "upscale"
    | "controlnet_preprocess"
    | "controlnet_postprocess";
  upload?: ImageUpload;
  generation?: Image;
  filepath?: string;
  height?: number;
  width?: number;
}

interface ControlnetUnits {}

interface GenerationSend
  extends Any2imgRequest,
    UpscaleRequest,
    Img2imgRequest,
    IRateable {
  checkpoint_model?: ModelVersionSend;
  sd_model_checkpoint?: ModelVersionSend;
  hr_checkpoint_model?: ModelVersionSend;
  lora_models?: LoraVersionSend[];
  hr_lora_models?: LoraVersionSend[];
  embedding_models?: ModelVersionSend[];
  controlnet_models?: ModelVersionSend[];
  gen_id?: UUID;
  enable_hr?: boolean;
  group_id?: string;
  hr_scale?: number;
  file_list?: FileList;
  init_images?: (ImageReference | RawImage | string)[];
  init_image_paths?: string[];
  controlnet_units?: ControlnetUnits[];
  model_id?: number;
  url?: string;

  controlnet_preprocesses?: ImageReference[];
  controlnet_postprocess?: ImageReference[];
}

interface Batch {
  name: string;
  generations: GenerationSend[];
  generation_overrides: {
    group_id: string;
    hide: boolean;
  } & Partial<GenerationSend>;
}

interface ControlnetArgs {
  input_image: ImageReference | RawImage | string;
  mask?: string;
  module: string;
  model: string;
  weight: number;
  resize_mode: string;
  lowvram: boolean;
  processor_res: number;
  threshold_a: number;
  threshold_b: number;
  guidance: number;
  guidance_start: number;
  guidance_end: number;
  guessmode: boolean;
  pixel_perfect: boolean;
}

interface AlwaysonScripts {
  controlnet?: { args: ControlnetArgs[] };
  [key: string]: any;
}

interface ModelReference {
  id: UUID;
  model: any;
}

/**
 * @interface Image
 * This describes any images generated,
 * has some excess properties based on type of request
 */
interface Image extends UpscaleRequest, Txt2imgRequest, Img2imgRequest {
  blur_hash?: string;
  filepath?: string;
  id?: UUID;
  tags?: Tag[];
  batch?: GenerationBatch;

  script_args?: string[];
  script_name?: string;

  controlnet_image_id?: UUID;
  controlnet_image?: any;

  updatedAt?: Date;
  createdAt?: Date;

  upload_image?: ImageReference[];
  controlnet_preprocesses?: ImageReference[];
  controlnet_postprocess?: ImageReference[];
  img2img_image?: ImageReference[];

  ratings?: IUserRating[];
  group_id?: string;
  user?: User;

  controlnet_models?: ImageModel[];
  checkpoint_model?: ImageModel;
  hr_checkpoint_model?: ImageModel;
  lora_models?: ImageModel[];
  hr_lora_models?: ImageModel[];
  embedding_models?: ImageModel[];
  parameters?: any;
}

interface Tag {
  id: UUID;
  user_id?: UUID;
  name: string;
  description?: string;
  private?: boolean;
}

interface GenerationBatch {
  id: UUID;
  user_id: UUID;
  images?: Image[];
  createdAt?: Date;
}

interface User {
  id: UUID;
  name: string;
  first_name: string;
  last_name: string;
  email: string;
  password: string;
  registration_key: string;
  reset_password_key: string;
  registration_id: string;
  token?: string;
  groups?: Group[];
}

interface ImageTag {
  tag_id: UUID;
  image_id: UUID;
}

interface StatusWebResponse {
  running_workers: number;
  queueSize: number;
  progress: number;
  generations: any[];
  current_generations: any[];
  eta: number;
}

export const enum WebSocketMessageType {
  ERROR = "ERROR",
  STATUS = "STATUS",
  USER = "USER",
  UPDATE = "UPDATE",
  SAFTEY = "SAFTEY",
  UNAUTHENTICATED = "UNAUTHENTICATED",
  WS_INFO = "WS_INFO",
  WORKER_INFO = "WORKER_INFO",
}

interface WebSocketMessage {
  type: WebSocketMessageType;
  user_id?: UUID | null;
  data: string | User | StatusWebResponse | any;
}

interface UserGroup {
  user_id: UUID;
  group_id: UUID;
  is_owner: boolean;
}

interface ImageGroup {
  image_id: UUID;
  group_id: UUID;
}

interface Group {
  id: UUID;
  name: string;
  description: string;
  images?: Image[];
  members?: User[];
  is_public?: boolean;
  user_groups?: UserGroup[];
  image_groups?: ImageGroup[];
  default_image?: Image;
}

interface MachineLog {
  id?: UUID;
  machine_id: UUID;
  batch_id?: UUID;
  type: string;
  message: string;
}

interface Machine {
  id?: UUID;
  name: string;
  description: string;
  is_public: boolean;
  machine_endpoint?: string;
  owner?: User;
  owner_id?: UUID;
  logs?: MachineLog[];
  users?: User[];
}

interface FeedbackCreationRequest {
  description: string;
  location: string;
  type: "bug" | "feedback" | "feature request" | "other";
}

interface FeedbackResponse {
  id: UUID;
  description: string;
  location: string;
  type: "bug" | "feedback" | "feature request" | "other";
  resolved: boolean;
  priority: number;
}

interface FeedbackFilterRequest {
  user_id?: UUID;
  type?: "bug" | "feedback" | "feature request" | "other";
  resolved?: boolean;
  priority?: number;
  page_size?: number;
  page?: number;
  order?: "priority" | "createdAt";
}

interface FeedbackUpdateRequest {
  id?: UUID;
  description?: string;
  location?: string;
  type?: "bug" | "feedback" | "feature request" | "other";
  resolved?: boolean;
  priority?: number;
}

interface CivitaiModel {
  uuid?: UUID;
  id?: number;

  source_url?: string;
  name?: string;
  description?: string;
  type?: string;
  poi?: boolean;
  nsfw?: boolean;
  allowNoCredit?: boolean;
  allowCommercialUse?: string;
  allowDerivatives?: boolean;
  allowDifferentLicens?: boolean;
  creator?: any;
  tags?: string[];
  modelVersions?: CivitaiModelVersion[];

  ratings?: IUserRating[];
  my_rating?: IUserRating;

  users?: User[];
  user_models?: UserModel[];

  image_rating_average?: number;
  rating_average?: number;
  rating_count?: number;
  stats?: any;
}
interface CivitaiModelVersion {
  uuid?: UUID;
  id?: number;

  modelUUID?: UUID;
  modelId?: number;
  model?: CivitaiModel;
  name?: string;
  createdAt?: Date;
  updatedAt?: Date;
  trainedWords?: string[];
  description?: string;
  downloadUrl?: string;
  baseModel?: string;
  files?: CivitaiFile[];
  images?: CivitaiImage[];

  ratings?: IUserRating[];
  my_rating?: IUserRating;

  rating_average?: number;
  rating_count?: number;
  stats?: any;

  filename: string;
  ext: string;
}

interface CivitaiImage {
  uuid?: UUID;
  id?: number;

  modelVersionUUID?: UUID;
  modelVersionId?: number;
  modelVersion?: CivitaiModelVersion;
  url: string;
  nsfw: boolean;
  nsfwLevel?: string;
  width: number;
  height: number;
  hash: string;
  meta: any;
}
interface UserModel {
  user_id?: UUID;
  user?: User;
  model_id?: UUID;
  model_id_number?: number;

  model?: CivitaiModel;
  default_weight?: number;
  notes?: string;
  default_version_id_number?: number;
  default_version_id?: UUID;

  default_version?: CivitaiModelVersion;
  favorite?: boolean;

  trained_words?: string[];
  trained_words_all?: string[];

  display_name: string;
  nsfw_override?: boolean;
}

interface ImageModel {
  image_id?: UUID;
  image?: Image;
  model_id?: UUID;
  model?: CivitaiModelVersion;
  display_name: string;
  type: string;
  filename: string;
  ext: string;
}

interface CivitaiImage {
  uuid?: UUID;
  id?: number;

  modelVersionUUID?: UUID;
  modelVersionId?: number;
  modelVersion?: CivitaiModelVersion;
  url: string;
  nsfw: boolean;
  nsfwLevel?: string;
  width: number;
  height: number;
  hash: string;
  meta: any;
}
interface UserModel {
  user_id?: UUID;
  user?: User;
}

interface Label {
  id?: UUID;
  label: string;
  nsfw_level?: "None" | "Soft" | "Mature" | "X";
}

interface CivitaiFile {
  uuid?: UUID;
  id?: number;
  modelVersionUUID?: UUID;
  modelVersion: CivitaiModelVersion;
  name?: string;
  base_name?: string;
  sizeKB?: number;
  type?: string;
  format?: string;
  pickleScanResult?: string;
  pickleScanMessage?: string;
  virusScanResult?: string;
  scannedAt?: Date;
  hashes?: any;
  downloadUrl?: string;
  metadata?: any;
  primary?: boolean;
}

interface ModelVersionSend extends CivitaiModelVersion {
  display_name: string;
  type: string;
  selected_version?: CivitaiModelVersion;
  ext: string;
  default_version?: CivitaiModelVersion;
}

interface LoraVersionSend extends ModelVersionSend {
  multiplier: number;
  trained_words_all?: string[];
}

interface LoraVersion {
  name: string;
  display_name: string;
  id?: number;
  uuid?: UUID;
  createdAt?: string;
  updatedAt?: string;
  type: string;
  filename: string;
  version_name: string;
  model_id: string;
  version_id: string;
  multiplier: number;
  download_url: string;
  trainedWords: string[];
}
